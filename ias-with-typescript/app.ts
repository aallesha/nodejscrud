
import dotenv from 'dotenv';
dotenv.config()

require("./config/database")
import server from './config/server'

const port = process.env.PORT || 5000;

server.listen(port, () => {
  console.log(`app running on port ${port}`);
});