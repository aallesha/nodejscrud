import mongoose, { Schema, model } from 'mongoose'

interface UserDetail {
    user_id: object;
    first_name: string;
    middle_name: string;
    last_name: string;
    dob: Date;
    gender_id: number;
    current_status: number;
    batch: number;
    is_address_same: number;
    temporary_address: string;
    temporary_district: number;
    permanent_address: string;
    permanent_district: number;

}

const UserDetailSchema = new Schema<UserDetail>(
    {
        user_id: {
            type: mongoose.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        first_name: {
            type: String,
            required: true,
        },
        middle_name: {
            type: String,
        },
        last_name: {
            type: String,
        },
        dob: {
            type: Date
        },
        gender_id: {
            type: Number
        },
        current_status: {
            type: Number
        },
        batch: {
            type: Number 
        },
        is_address_same: {
            type: Number,
            default: 0
        },
        temporary_address: {
            type: String
        },
        temporary_district: {
            type: Number
        },
        permanent_address: {
            type: String
        },
        permanent_district: {
            type: Number
        },
    }
)

const UserDetail = model<UserDetail>('UserDetail', UserDetailSchema)

export default UserDetail