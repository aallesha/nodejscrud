import { Schema, model } from 'mongoose'

interface UserVerification {
    user_id: object;
    verification_code: string,
    for: number,
    expire: Date
}

const UserVerificationSchema = new Schema<UserVerification>(
    {
        user_id: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'User',
        },
        verification_code: {
            type: String,
            required: true
        },
        for: {
            type: Number,
            required: true
        },
        expire: {
            type: Date,
            required: true
        }
    }, 
    {
        timestamps: true
    }
)

const UserVerification = model<UserVerification>('UserVerification', UserVerificationSchema)

export { UserVerification, UserVerificationSchema }