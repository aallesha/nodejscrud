import User from '../models/user.model'
import { UserVerification } from '../models/user_verification.model'
import dotenv from 'dotenv';
dotenv.config()
import { validateUser, validateLoginDetails, validateOTPDetails, validateResendOTPDetails, validateResetPasswordDetails } 
from '../validations/user'
import bcrypt from 'bcrypt';
import transporter from '../helpers/sendmail'
import {Request, Response} from 'express';

class UserController {

    register = async (req: Request, res: Response) => {
        let { name, email, mobile, password } = req.body
        
        let errors = validateUser(req.body);

        if (Object.keys(errors).length === 0) {

            const userdata = await User.findOne({ "email" : email })
            if (userdata) {
                errors.email = 'Email already exists'
                return res.status(400).json({ message: "", errors: errors, status: 400 })
            }

            const salt = await bcrypt.genSalt(10);
            password = await bcrypt.hash(password, salt);

            try {
                const confirmationCode = Math.floor((Math.random() * 9999) + 1111);

                interface UserData {
                    name: string;
                    email: string;
                    mobile: string;
                    password: string;
                    email_verified_at: null;
                    is_approve: number;
                }

                let userData = <UserData>{
                    name: name,
                    email: email,
                    mobile: mobile,
                    password: password,
                    email_verified_at: null,
                    is_approve: 1,
                }


                const result = await User.create(userData, function(err: any, response: any){
                    
                    //add otp data
                    const d = new Date()
                    const expiryDate = d.setHours(d.getHours() + 1)
                    UserVerification.create({"user_id": response._id, "verification_code": confirmationCode, "for": 1, expire: expiryDate})
                    
                    //send otp mail
                    try {
                        
                        transporter.sendMail({
                            from: 'ias@no-reply.com',
                            to: 'aallesha.fulzele@maximess.com',
                            subject: "Please confirm your account",
                            html: `<h2>IAS OTP Confirmation</h2>
                                <h3>Hello ${name}</h3>
                                <p>Thank you for registering. Your OTP is ${confirmationCode}</p>
                                </div>`,
                        }).catch(err => {
                            //delete user entry if mail not sent
                            User.remove({_id: response._id})
                            UserVerification.remove({user_id: response._id})
                            res.status(400).json({message: "User cannot be registered. Please try agian.", errors: {}, status: 400});
                        });
                       
                        res.status(200).json({message:'User registered successfully', data:response, errors:{}, status:200});
                        
                    } catch (e) {
                        //delete user entry if mail not sent
                        User.remove({_id: response._id})
                        UserVerification.remove({user_id: response._id})
                        res.status(400).json({message: "User cannot be registered. Please try agian.", errors: {}, status: 400});
                    }
                });
                
            } catch (e) {
                res.status(400).json({message: "User cannot be registered. Please try agian.", errors: {}, status: 400});
            }
                
        } else {
            res.status(400).json({message: "Invalid input", errors: errors, status: 400});
        } 
    }

    login = async(req: Request, res: Response) => {
        let { email, password } = req.body

        try {
            let errors = validateLoginDetails(req.body);
        
            if (Object.keys(errors).length === 0) { 
                const user = await User.findOne({"email": email});
        
                if (user && (await bcrypt.compare(password, user.password))) {
                    
                    if(user.is_approve == 0) {
                        res.status(400).json({message:"Please ask admin to approve your account", errors:{} });
                    } else if(user.email_verified_at == null) {
                        res.status(400).json({message:"OTP confirmation not done", errors:{} });
                    }
                
                    const token = user.generateAuthToken();

                    res.status(200).json({message:"Login sucessfull", data:user, token:token, errors:{}, status:200 }); 
                } else {

                    errors.password = "Invalid credentials"

                    res.status(400).json({message:"Login not sucessfull", errors:errors });
                }
            } else {
                res.status(400).json({message: "Invalid input", errors: errors, status: 400});
            }
        } catch(e) {
            res.status(400).json({message: "Some error occured", errors: e.message, status: 400});
        }
    }

    verifyOTP = async(req: Request, res: Response) => {
        let { email, verification_code } = req.body

        let errors = validateOTPDetails(req.body);
        
        if (Object.keys(errors).length === 0) { 

            User
            .findOne({email: email})
            .populate('userverification')
            .then(user => {
                if(!user) {
                    res.status(400).json({message: "Email Id does not exist", errors: {}, status: 400});
                } else {
                    if(user.userverification) {
                        let expiryDate = new Date(user.userverification.expire);
                        let currentDate = new Date();

                        if(expiryDate < currentDate) {
                            res.status(400).json({message: "OTP expired", errors: {}, status: 400});
                        } else if(user.userverification.verification_code != verification_code) {
                            res.status(400).json({message: "Incorrect OTP", errors: {}, status: 400});
                        } else {
                            User.findByIdAndUpdate({ _id: user._id }, { email_verified_at: Date() }, async function (err, response) {
                                res.status(200).json({message:"OTP Verified successfully", data:user, errors:{}, status: 200 }); 
                            })
                        }
                    } else {
                        res.status(400).json({message: "Invalid data", errors: {}, status: 400});
                    }
                }
            });
     
        } else {
            res.status(400).json({message: "Invalid input", errors: errors, status: 400});
        }
    }

    resendOTP = async(req: Request, res: Response) => {
        let email = req.body.email
        
        let errors = validateResendOTPDetails(req.body);
        
        if (Object.keys(errors).length === 0) { 
            
            
            User.findOne({ email: email}, (err: any, response: any) => {
               
                if(response == null) {
                    res.status(400).json({message: "Email does not exist", errors: {}, status: 400});
                } else {
                    const confirmationCode = Math.floor((Math.random() * 9999) + 1111);

                    // change expiry datetime and new otp
                    const d = new Date()
                    const expiryDate = d.setHours(d.getHours() + 1)
                    UserVerification.findOneAndUpdate({ user_id: response._id}, { expire: expiryDate, verification_code: confirmationCode }, {new: true}, (err, response) => {
                       
                    })
                   
                    //send otp mail
                    try {
                        transporter.sendMail({
                            from: 'ias@no-reply.com',
                            to: 'aallesha.fulzele@maximess.com',
                            subject: "Please confirm your account",
                            html: `<h1>IAS OTP Confirmation</h1>
                                <h2>Hello ${response.name}</h2>
                                <p>Thank you for registering. Your OTP is ${confirmationCode}</p>
                                </div>`,
                        }).catch(err => {
                            res.status(400).json({message: "Mail not sent. Please try agian.", errors: {}, status: 400});
                        });
                        
                        res.status(200).json({message:'Success', errors:{}, status:200});
                        
                    } catch (e) {
                        
                        res.status(400).json({message: "Mail not sent. Please try agian.", errors: {}, status: 400});
                    }
                }
            })

        } else {
            res.status(400).json({message: "Invalid input", errors: errors, status: 400});
        }
    }

    forgetPasswordOTP = async(req: Request, res: Response) => {
       
        let errors = {} as any;
        console.log(req.body)

        //check required fields
        if(!req.body.email) {
            errors.email = "The email field is required."
        }
        
        if (Object.keys(errors).length === 0) { 
            let email = req.body.email

            User.findOne({ email: email}, (err: any, user: any) => {
                if(!user) {
                    res.status(400).json({message: "Requested Email not exists.", errors: {}, status: false});
                } else {
                    const confirmationCode = Math.floor((Math.random() * 9999) + 1111);
                    
                    // change expiry datetime and new otp
                    const d = new Date()
                    const expiryDate = d.setHours(d.getHours() + 1)

                    UserVerification.findOneAndUpdate({ user_id: user._id}, { expire: expiryDate, verification_code: confirmationCode }, {new: true}, (err, response) => {
                        //send otp mail
                        try {
                            transporter.sendMail({
                                from: 'ias@no-reply.com',
                                to: 'aallesha.fulzele@maximess.com',
                                subject: "Forgot password OTP",
                                html: `<h1>IAS Forgot Password OTP Confirmation</h1>
                                    <h2>Hello ${user.name}</h2>
                                    <p>Thank you for requesting change password. Your OTP is ${confirmationCode}</p>
                                    </div>`,
                            }).catch(err => {
                                res.status(400).json({message: "Mail not sent. Please try agian.", errors: {}, status: false});
                            });
                            
                            res.status(200).json({message:'Forget otp sent', errors:{}, status: true, data: {email: email}});
                            
                        } catch (e) {
                            res.status(400).json({message: "Mail not sent. Please try agian.", errors: e.message, status: false});
                        }
                    })
                }
            })
        } else {
            res.status(400).json({message: "Invalid input", errors: errors, status: false});
        }
    }

    resetPassword = async(req: Request, res: Response) => {
        let errors = validateResetPasswordDetails(req.body);
        
        //check if input data is valid
        if (Object.keys(errors).length === 0) {

            let { email, verification_code, password } = req.body;
            const salt = await bcrypt.genSalt(10);
            password = await bcrypt.hash(password, salt);


            //find user and otp details
            User
            .findOne({email: email})
            .populate('userverification')
            .then(user => {
                if(!user) {
                    res.status(400).json({message: "Requested Email not exists.", data: [], status: false});
                } else {
                    if(user.userverification) {
                        let expiryDate = new Date(user.userverification.expire);
                        let currentDate = new Date();

                        if(expiryDate < currentDate) {
                            res.status(400).json({message: "Verification code expired", data: [], status: false});
                        } else if(user.userverification.verification_code != verification_code) {
                            res.status(400).json({message: "Verification code not match", data: [], status: false});
                        } else {
                            
                            User.findByIdAndUpdate({ _id: user._id }, { password: password }, (err, result) => {
                                if(result) {
                                    res.status(200).json({ message: "Password Reset Successfully", data: req.body, status: true }); 
                                } else {
                                    res.status(400).json({message: err, data: [], status: false}); 
                                }
                            })
                        }
                    } else {
                        res.status(400).json({message: "Invalid data", data: [], status: false});
                    }
                }
            });
        } else {
            res.status(400).json({message: errors, data: [], status: false});
        }
    }
}

export default new UserController()