import UserDetail from '../models/user_detail.model'
import {validateAddUserDetail} from '../validations/user_detail'
import {Request, Response} from 'express';
const mongoose = require('mongoose')

class UserDetailController {

    addUserDetail = async(req: Request | any, res: Response) => {
        try {

            let errors = validateAddUserDetail(req.body)

            if(Object.keys(errors).length === 0) {

                req.body.user_id = req.user.user_id

                UserDetail.create(req.body, (err: any, result: any) => {
                    if(err) {
                        res.status(400).json({ status: false, message: err, data:[] })
                    } else {
                        res.status(200).json({ status: true, message: "User details added successfully", data: result })
                    }
                })
            } else {
                res.status(400).json({status: false, message: errors, data: []})
            }
        } catch(e) {
            res.status(400).json({status: false, message: 'Some error occured', data: []})
        }
    }

    getUserDetail = async(req: Request | any, res: Response) => {
        try {

            let errors = {} as any

            if(!req.params.id) {
                errors.id = "User Detail id is required"
            }

            if(Object.keys(errors).length === 0) {

                req.body.user_id = req.user.user_id
                
                UserDetail.findById(req.params.id, (err: any, result: any) => {
                    if(err) {
                        res.status(400).json({status: false, message: err, data:[]})
                    } else {
                        res.status(200).json({status: true, message: "Success", data: result})
                    }
                })
            } else {
                res.status(400).json({status: false, message: errors, data: []})
            }
        } catch(e) {
            res.status(400).json({status: false, message: e.message, data: []})
        }
    }

    updateUserDetail = async(req: Request | any, res: Response) => {
        try {

            let errors = validateAddUserDetail(req.body)

            if(!req.params.id){
                errors.id = "User Detail id is required"
            }

            if(Object.keys(errors).length === 0) {

                req.body.user_id = req.user.user_id

                UserDetail.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err: any, result: any) => {
                    if(err) {
                        res.status(400).json({ status: false, message: err, data:[] })
                    } else {
                        res.status(200).json({ status: true, message: "User details updated successfully", data: result })
                    }
                })
            } else {
                res.status(400).json({status: false, message: errors, data: []})
            }
        } catch(e) {
            res.status(400).json({status: false, message: e.message, data: []})
        }
    }

    deleteUserDetail = async(req: Request | any, res: Response) => {
        try {

            let errors = {} as any

            if(!req.params.id){
                errors.id = "User Detail id is required"
            }

            if(Object.keys(errors).length === 0) {

                const id = mongoose.Types.ObjectId(req.params.id);

                UserDetail.findOneAndDelete(id, (err: any, result: any) => {
                    if(err) {
                        res.status(400).json({ status: false, message: err, data:[]})
                    } else {
                        res.status(200).json({ status: true, message: "Deleted successfully", data: []})
                    }
                })
            } else {
                res.status(400).json({status: false, message: errors, data: []})
            }

        } catch(e) {
            res.status(400).json({status: false, message: 'Some error occured', data: []})
        }
    }
}

export default new UserDetailController()