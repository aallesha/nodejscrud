import express from 'express' 
const router = express.Router()

import auth from '../middleware/auth'
import UserDetailController from '../controllers/user_detail.controller'

router.post('/api/iasuserdetails', auth, UserDetailController.addUserDetail)
router.get('/api/user-details/:id', auth, UserDetailController.getUserDetail)
router.patch('/api/user-details/:id', auth, UserDetailController.updateUserDetail)
router.delete('/api/user-details/:id', auth, UserDetailController.deleteUserDetail)

export default router