interface UserDetail {
    first_name: string
}

const validateAddUserDetail = (data: UserDetail) => {
    let errors = {} as any

    if(!data.first_name){
        errors.first_name = "First Name is required"
    }

    return errors
}

export { validateAddUserDetail }