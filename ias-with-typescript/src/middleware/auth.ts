import dotenv from 'dotenv';
dotenv.config() 

import jwt from 'jsonwebtoken'
import {Request, Response} from 'express';

const auth = (req: Request | any, res: Response, next: any) => {
    try {
        let token = req.headers.authorization
        token = token.replace(/^Bearer\s+/, ""); 

        if (!token) return res.status(403).send("Access denied.");

        const decoded = jwt.verify(token, process.env.TOKEN_KEY);
        req.user = decoded;
        next();
    } catch (error) {
        res.status(400).send("Invalid token");
    }
};

export default auth