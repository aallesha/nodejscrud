"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const user_controller_1 = __importDefault(require("../controllers/user.controller"));
const router = express_1.default.Router();
router.post('/api/register', user_controller_1.default.register);
router.post('/api/login', user_controller_1.default.login);
router.post('/api/otp-verify', user_controller_1.default.verifyOTP);
router.post('/api/resend-otp', user_controller_1.default.resendOTP);
router.post('/api/reset-password/send-otp', user_controller_1.default.forgetPasswordOTP);
router.put('/api/reset-password', user_controller_1.default.resetPassword);
exports.default = router;
//# sourceMappingURL=user.route.js.map