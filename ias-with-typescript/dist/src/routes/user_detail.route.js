"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
const auth_1 = __importDefault(require("../middleware/auth"));
const user_detail_controller_1 = __importDefault(require("../controllers/user_detail.controller"));
router.post('/api/iasuserdetails', auth_1.default, user_detail_controller_1.default.addUserDetail);
router.get('/api/user-details/:id', auth_1.default, user_detail_controller_1.default.getUserDetail);
router.patch('/api/user-details/:id', auth_1.default, user_detail_controller_1.default.updateUserDetail);
router.delete('/api/user-details/:id', auth_1.default, user_detail_controller_1.default.deleteUserDetail);
exports.default = router;
//# sourceMappingURL=user_detail.route.js.map