"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const auth = (req, res, next) => {
    try {
        let token = req.headers.authorization;
        token = token.replace(/^Bearer\s+/, "");
        if (!token)
            return res.status(403).send("Access denied.");
        const decoded = jsonwebtoken_1.default.verify(token, process.env.TOKEN_KEY);
        req.user = decoded;
        next();
    }
    catch (error) {
        res.status(400).send("Invalid token");
    }
};
exports.default = auth;
//# sourceMappingURL=auth.js.map