"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserVerificationSchema = exports.UserVerification = void 0;
const mongoose_1 = require("mongoose");
const UserVerificationSchema = new mongoose_1.Schema({
    user_id: {
        type: mongoose_1.Schema.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    verification_code: {
        type: String,
        required: true
    },
    for: {
        type: Number,
        required: true
    },
    expire: {
        type: Date,
        required: true
    }
}, {
    timestamps: true
});
exports.UserVerificationSchema = UserVerificationSchema;
const UserVerification = (0, mongoose_1.model)('UserVerification', UserVerificationSchema);
exports.UserVerification = UserVerification;
//# sourceMappingURL=user_verification.model.js.map