"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require('jsonwebtoken');
const mongoose_1 = require("mongoose");
const UserSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    mobile: {
        type: String,
        required: true,
    },
    email_verified_at: {
        type: Date,
    },
    password: {
        type: String
    },
    is_approve: {
        type: Number
    },
    remember_token: {
        type: String
    }
}, {
    timestamps: true
});
UserSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({ user_id: this._id, email: this.email }, process.env.TOKEN_KEY);
    return token;
};
UserSchema.virtual('userverification', {
    ref: 'UserVerification',
    localField: '_id',
    foreignField: 'user_id',
    justOne: true
});
UserSchema.set('toObject', { virtuals: true });
UserSchema.set('toJSON', { virtuals: true });
const User = (0, mongoose_1.model)("User", UserSchema);
exports.default = User;
//# sourceMappingURL=user.model.js.map