"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateAddUserDetail = void 0;
const validateAddUserDetail = (data) => {
    let errors = {};
    if (!data.first_name) {
        errors.first_name = "First Name is required";
    }
    return errors;
};
exports.validateAddUserDetail = validateAddUserDetail;
//# sourceMappingURL=user_detail.js.map