"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_detail_model_1 = __importDefault(require("../models/user_detail.model"));
const user_detail_1 = require("../validations/user_detail");
const mongoose = require('mongoose');
class UserDetailController {
    constructor() {
        this.addUserDetail = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                let errors = (0, user_detail_1.validateAddUserDetail)(req.body);
                if (Object.keys(errors).length === 0) {
                    req.body.user_id = req.user.user_id;
                    user_detail_model_1.default.create(req.body, (err, result) => {
                        if (err) {
                            res.status(400).json({ status: false, message: err, data: [] });
                        }
                        else {
                            res.status(200).json({ status: true, message: "User details added successfully", data: result });
                        }
                    });
                }
                else {
                    res.status(400).json({ status: false, message: errors, data: [] });
                }
            }
            catch (e) {
                res.status(400).json({ status: false, message: 'Some error occured', data: [] });
            }
        });
        this.getUserDetail = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                let errors = {};
                if (!req.params.id) {
                    errors.id = "User Detail id is required";
                }
                if (Object.keys(errors).length === 0) {
                    req.body.user_id = req.user.user_id;
                    user_detail_model_1.default.findById(req.params.id, (err, result) => {
                        if (err) {
                            res.status(400).json({ status: false, message: err, data: [] });
                        }
                        else {
                            res.status(200).json({ status: true, message: "Success", data: result });
                        }
                    });
                }
                else {
                    res.status(400).json({ status: false, message: errors, data: [] });
                }
            }
            catch (e) {
                res.status(400).json({ status: false, message: e.message, data: [] });
            }
        });
        this.updateUserDetail = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                let errors = (0, user_detail_1.validateAddUserDetail)(req.body);
                if (!req.params.id) {
                    errors.id = "User Detail id is required";
                }
                if (Object.keys(errors).length === 0) {
                    req.body.user_id = req.user.user_id;
                    user_detail_model_1.default.findByIdAndUpdate(req.params.id, req.body, { new: true }, (err, result) => {
                        if (err) {
                            res.status(400).json({ status: false, message: err, data: [] });
                        }
                        else {
                            res.status(200).json({ status: true, message: "User details updated successfully", data: result });
                        }
                    });
                }
                else {
                    res.status(400).json({ status: false, message: errors, data: [] });
                }
            }
            catch (e) {
                res.status(400).json({ status: false, message: e.message, data: [] });
            }
        });
        this.deleteUserDetail = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                let errors = {};
                if (!req.params.id) {
                    errors.id = "User Detail id is required";
                }
                if (Object.keys(errors).length === 0) {
                    const id = mongoose.Types.ObjectId(req.params.id);
                    user_detail_model_1.default.findOneAndDelete(id, (err, result) => {
                        if (err) {
                            res.status(400).json({ status: false, message: err, data: [] });
                        }
                        else {
                            res.status(200).json({ status: true, message: "Deleted successfully", data: [] });
                        }
                    });
                }
                else {
                    res.status(400).json({ status: false, message: errors, data: [] });
                }
            }
            catch (e) {
                res.status(400).json({ status: false, message: 'Some error occured', data: [] });
            }
        });
    }
}
exports.default = new UserDetailController();
//# sourceMappingURL=user_detail.controller.js.map