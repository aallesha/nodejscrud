"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_model_1 = __importDefault(require("../models/user.model"));
const user_verification_model_1 = require("../models/user_verification.model");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const user_1 = require("../validations/user");
const bcrypt_1 = __importDefault(require("bcrypt"));
const sendmail_1 = __importDefault(require("../helpers/sendmail"));
class UserController {
    constructor() {
        this.register = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let { name, email, mobile, password } = req.body;
            let errors = (0, user_1.validateUser)(req.body);
            if (Object.keys(errors).length === 0) {
                const userdata = yield user_model_1.default.findOne({ "email": email });
                if (userdata) {
                    errors.email = 'Email already exists';
                    return res.status(400).json({ message: "", errors: errors, status: 400 });
                }
                const salt = yield bcrypt_1.default.genSalt(10);
                password = yield bcrypt_1.default.hash(password, salt);
                try {
                    const confirmationCode = Math.floor((Math.random() * 9999) + 1111);
                    let userData = {
                        name: name,
                        email: email,
                        mobile: mobile,
                        password: password,
                        email_verified_at: null,
                        is_approve: 1,
                    };
                    const result = yield user_model_1.default.create(userData, function (err, response) {
                        //add otp data
                        const d = new Date();
                        const expiryDate = d.setHours(d.getHours() + 1);
                        user_verification_model_1.UserVerification.create({ "user_id": response._id, "verification_code": confirmationCode, "for": 1, expire: expiryDate });
                        //send otp mail
                        try {
                            sendmail_1.default.sendMail({
                                from: 'ias@no-reply.com',
                                to: 'aallesha.fulzele@maximess.com',
                                subject: "Please confirm your account",
                                html: `<h2>IAS OTP Confirmation</h2>
                                <h3>Hello ${name}</h3>
                                <p>Thank you for registering. Your OTP is ${confirmationCode}</p>
                                </div>`,
                            }).catch(err => {
                                //delete user entry if mail not sent
                                user_model_1.default.remove({ _id: response._id });
                                user_verification_model_1.UserVerification.remove({ user_id: response._id });
                                res.status(400).json({ message: "User cannot be registered. Please try agian.", errors: {}, status: 400 });
                            });
                            res.status(200).json({ message: 'User registered successfully', data: response, errors: {}, status: 200 });
                        }
                        catch (e) {
                            //delete user entry if mail not sent
                            user_model_1.default.remove({ _id: response._id });
                            user_verification_model_1.UserVerification.remove({ user_id: response._id });
                            res.status(400).json({ message: "User cannot be registered. Please try agian.", errors: {}, status: 400 });
                        }
                    });
                }
                catch (e) {
                    res.status(400).json({ message: "User cannot be registered. Please try agian.", errors: {}, status: 400 });
                }
            }
            else {
                res.status(400).json({ message: "Invalid input", errors: errors, status: 400 });
            }
        });
        this.login = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let { email, password } = req.body;
            try {
                let errors = (0, user_1.validateLoginDetails)(req.body);
                if (Object.keys(errors).length === 0) {
                    const user = yield user_model_1.default.findOne({ "email": email });
                    if (user && (yield bcrypt_1.default.compare(password, user.password))) {
                        if (user.is_approve == 0) {
                            res.status(400).json({ message: "Please ask admin to approve your account", errors: {} });
                        }
                        else if (user.email_verified_at == null) {
                            res.status(400).json({ message: "OTP confirmation not done", errors: {} });
                        }
                        const token = user.generateAuthToken();
                        res.status(200).json({ message: "Login sucessfull", data: user, token: token, errors: {}, status: 200 });
                    }
                    else {
                        errors.password = "Invalid credentials";
                        res.status(400).json({ message: "Login not sucessfull", errors: errors });
                    }
                }
                else {
                    res.status(400).json({ message: "Invalid input", errors: errors, status: 400 });
                }
            }
            catch (e) {
                res.status(400).json({ message: "Some error occured", errors: e.message, status: 400 });
            }
        });
        this.verifyOTP = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let { email, verification_code } = req.body;
            let errors = (0, user_1.validateOTPDetails)(req.body);
            if (Object.keys(errors).length === 0) {
                user_model_1.default
                    .findOne({ email: email })
                    .populate('userverification')
                    .then(user => {
                    if (!user) {
                        res.status(400).json({ message: "Email Id does not exist", errors: {}, status: 400 });
                    }
                    else {
                        if (user.userverification) {
                            let expiryDate = new Date(user.userverification.expire);
                            let currentDate = new Date();
                            if (expiryDate < currentDate) {
                                res.status(400).json({ message: "OTP expired", errors: {}, status: 400 });
                            }
                            else if (user.userverification.verification_code != verification_code) {
                                res.status(400).json({ message: "Incorrect OTP", errors: {}, status: 400 });
                            }
                            else {
                                user_model_1.default.findByIdAndUpdate({ _id: user._id }, { email_verified_at: Date() }, function (err, response) {
                                    return __awaiter(this, void 0, void 0, function* () {
                                        res.status(200).json({ message: "OTP Verified successfully", data: user, errors: {}, status: 200 });
                                    });
                                });
                            }
                        }
                        else {
                            res.status(400).json({ message: "Invalid data", errors: {}, status: 400 });
                        }
                    }
                });
            }
            else {
                res.status(400).json({ message: "Invalid input", errors: errors, status: 400 });
            }
        });
        this.resendOTP = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let email = req.body.email;
            let errors = (0, user_1.validateResendOTPDetails)(req.body);
            if (Object.keys(errors).length === 0) {
                user_model_1.default.findOne({ email: email }, (err, response) => {
                    if (response == null) {
                        res.status(400).json({ message: "Email does not exist", errors: {}, status: 400 });
                    }
                    else {
                        const confirmationCode = Math.floor((Math.random() * 9999) + 1111);
                        // change expiry datetime and new otp
                        const d = new Date();
                        const expiryDate = d.setHours(d.getHours() + 1);
                        user_verification_model_1.UserVerification.findOneAndUpdate({ user_id: response._id }, { expire: expiryDate, verification_code: confirmationCode }, { new: true }, (err, response) => {
                        });
                        //send otp mail
                        try {
                            sendmail_1.default.sendMail({
                                from: 'ias@no-reply.com',
                                to: 'aallesha.fulzele@maximess.com',
                                subject: "Please confirm your account",
                                html: `<h1>IAS OTP Confirmation</h1>
                                <h2>Hello ${response.name}</h2>
                                <p>Thank you for registering. Your OTP is ${confirmationCode}</p>
                                </div>`,
                            }).catch(err => {
                                res.status(400).json({ message: "Mail not sent. Please try agian.", errors: {}, status: 400 });
                            });
                            res.status(200).json({ message: 'Success', errors: {}, status: 200 });
                        }
                        catch (e) {
                            res.status(400).json({ message: "Mail not sent. Please try agian.", errors: {}, status: 400 });
                        }
                    }
                });
            }
            else {
                res.status(400).json({ message: "Invalid input", errors: errors, status: 400 });
            }
        });
        this.forgetPasswordOTP = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let errors = {};
            console.log(req.body);
            //check required fields
            if (!req.body.email) {
                errors.email = "The email field is required.";
            }
            if (Object.keys(errors).length === 0) {
                let email = req.body.email;
                user_model_1.default.findOne({ email: email }, (err, user) => {
                    if (!user) {
                        res.status(400).json({ message: "Requested Email not exists.", errors: {}, status: false });
                    }
                    else {
                        const confirmationCode = Math.floor((Math.random() * 9999) + 1111);
                        // change expiry datetime and new otp
                        const d = new Date();
                        const expiryDate = d.setHours(d.getHours() + 1);
                        user_verification_model_1.UserVerification.findOneAndUpdate({ user_id: user._id }, { expire: expiryDate, verification_code: confirmationCode }, { new: true }, (err, response) => {
                            //send otp mail
                            try {
                                sendmail_1.default.sendMail({
                                    from: 'ias@no-reply.com',
                                    to: 'aallesha.fulzele@maximess.com',
                                    subject: "Forgot password OTP",
                                    html: `<h1>IAS Forgot Password OTP Confirmation</h1>
                                    <h2>Hello ${user.name}</h2>
                                    <p>Thank you for requesting change password. Your OTP is ${confirmationCode}</p>
                                    </div>`,
                                }).catch(err => {
                                    res.status(400).json({ message: "Mail not sent. Please try agian.", errors: {}, status: false });
                                });
                                res.status(200).json({ message: 'Forget otp sent', errors: {}, status: true, data: { email: email } });
                            }
                            catch (e) {
                                res.status(400).json({ message: "Mail not sent. Please try agian.", errors: e.message, status: false });
                            }
                        });
                    }
                });
            }
            else {
                res.status(400).json({ message: "Invalid input", errors: errors, status: false });
            }
        });
        this.resetPassword = (req, res) => __awaiter(this, void 0, void 0, function* () {
            let errors = (0, user_1.validateResetPasswordDetails)(req.body);
            //check if input data is valid
            if (Object.keys(errors).length === 0) {
                let { email, verification_code, password } = req.body;
                const salt = yield bcrypt_1.default.genSalt(10);
                password = yield bcrypt_1.default.hash(password, salt);
                //find user and otp details
                user_model_1.default
                    .findOne({ email: email })
                    .populate('userverification')
                    .then(user => {
                    if (!user) {
                        res.status(400).json({ message: "Requested Email not exists.", data: [], status: false });
                    }
                    else {
                        if (user.userverification) {
                            let expiryDate = new Date(user.userverification.expire);
                            let currentDate = new Date();
                            if (expiryDate < currentDate) {
                                res.status(400).json({ message: "Verification code expired", data: [], status: false });
                            }
                            else if (user.userverification.verification_code != verification_code) {
                                res.status(400).json({ message: "Verification code not match", data: [], status: false });
                            }
                            else {
                                user_model_1.default.findByIdAndUpdate({ _id: user._id }, { password: password }, (err, result) => {
                                    if (result) {
                                        res.status(200).json({ message: "Password Reset Successfully", data: req.body, status: true });
                                    }
                                    else {
                                        res.status(400).json({ message: err, data: [], status: false });
                                    }
                                });
                            }
                        }
                        else {
                            res.status(400).json({ message: "Invalid data", data: [], status: false });
                        }
                    }
                });
            }
            else {
                res.status(400).json({ message: errors, data: [], status: false });
            }
        });
    }
}
exports.default = new UserController();
//# sourceMappingURL=user.controller.js.map