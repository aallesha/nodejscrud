"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const mongoose_1 = __importDefault(require("mongoose"));
class Connection {
    constructor() {
        // const url = process.env.DB_CONNECTION || `mongodb+srv://aallesha:aalisha287%23@cluster0.mpeft.mongodb.net/ias?retryWrites=true&w=majority`;
        const url = `mongodb+srv://aallesha:aalisha287%23@cluster0.mpeft.mongodb.net/ias?retryWrites=true&w=majority`;
        mongoose_1.default.connect(url);
        mongoose_1.default.Promise = global.Promise;
        let db = mongoose_1.default.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));
    }
}
exports.default = new Connection();
//# sourceMappingURL=database.js.map