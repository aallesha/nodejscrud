"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const user_route_1 = __importDefault(require("../src/routes/user.route"));
// import userServiceDetailRoutes from '../src/routes/user_service_detail.route.js';
// import familyDetailRoutes from '../src/routes/family_detail.route.js'
const user_detail_route_1 = __importDefault(require("../src/routes/user_detail.route"));
// import miscRoutes from '../src/routes/misc.route.js'
// import serviceRequestRoutes from '../src/routes/service_request.route.js'
const server = express();
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
//routes
server.use(user_route_1.default);
// server.use(userServiceDetailRoutes)
// server.use(familyDetailRoutes)
server.use(user_detail_route_1.default);
// server.use(miscRoutes)
// server.use(serviceRequestRoutes)
exports.default = server;
//# sourceMappingURL=server.js.map