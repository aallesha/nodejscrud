"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
require("./config/database");
const server_1 = __importDefault(require("./config/server"));
const port = process.env.PORT || 5000;
server_1.default.listen(port, () => {
    console.log(`app running on port ${port}`);
});
//# sourceMappingURL=app.js.map