let arr =[6, 7, 8, 6, 12, 1, 2, 3, 4]
let endIndx = 0;
let maxLength = 0;
let indx = 1;
let tempMax = 0;

while (indx < arr.length) {
    if (arr[indx] > arr[indx - 1]) {
        tempMax++;
    } else {
        if (maxLength <= tempMax) {
            maxLength = tempMax+1
            endIndx = indx
            tempMax=0;
        }
    }
    ++indx
}

if (maxLength < tempMax) {
    maxLength = tempMax
    endIndx = indx
}

console.log("Sub array of consecutive numbers: ", arr.slice(endIndx-maxLength,endIndx))
