
var ar1 = [2, 3, 7, 10, 12]
var ar2 = [1, 5, 7, 8]

let i = 0, j = 0;
let result = 0, sum1 = 0, sum2 = 0;
let m = ar1.length
let n = ar2.length

while (i < m && j < n)
{
    if (ar1[i] < ar2[j]){
        sum1 += ar1[i++];
        //console.log(sum1)

    } else if (ar1[i] > ar2[j]) {
        sum2 += ar2[j++];
        //console.log(sum2)
    } else {
        
        result += Math.max(sum1, sum2) + ar1[i];
        //console.log(result)
        sum1 = 0;
        sum2 = 0;
        i++;
        j++;
        
    }
}

while (i < m) {
    sum1 += ar1[i++];
}

while (j < n) {
    sum2 += ar2[j++];
}

result += Math.max(sum1, sum2);

console.log("Maximum sum path is " + result);
