const express = require('express')
const parser = require('body-parser')

const app = express()
const port = 1235

app.use(parser.json())
app.use(parser.urlencoded({extended: false}))

app.listen(port, () => {

    const squareStar = () => {
        let n = 5;
        let string = ""
    
        for(let i = 0; i <= n; i++) {
            for(let j = 0; j <= n; j++){
                string += '*'
            }
            string += '\n'
        }
    
        console.log(string)
    }

    const hollowSquare = () => {
        // *****
        // *   *
        // *   *
        // *   *
        // *****

        let n = 5;
        let string = "";

        for(let i = 0; i <= n; i++) {
            for(let j=0; j<= n; j++){
                if(i == 0 || i == n) {
                    string += "*"
                } else {

                    if(j==0 || j == n) {
                        string += "*"
                    } else {
                        string += " "
                    }
                    //     string += "*"
                    // } else {
                    //     string += ""
                    // }
                }
                
            }
            string += '\n'
        }

        console.log(string)
    }

    const righttriangle = () => {
    //      *
    //     **
    //    ***
    //   ****
    //  *****
     
        let n = 5
        let string = ""

        for(let i=0; i<=n; i++){
            for (let j=0; j < n - i; j++){
                //console.log(j,n)
                string += " "
            }

            for(let k=0; k < i; k++){
                string += "*"
            }
            string += "\n"
        }
       
        console.log(string)
    }

   const lefttriandle = () => {
        // *
        // **
        // ***
        // ****
        // *****

        let n = 5;
        let string = ""

        for(let i=1; i<=n; i++) {
            for (let j=0;j< i;j++) {
                string += "*"
            }
            string += "\n"
        }
        console.log(string)
   }

   const downtriangle = () => {
        // *****
        // ****
        // ***
        // **
        // *
        let n = 5;
        let string = ""

        for(i=0; i<n; i++){
            for (j=0; j< n - i; j++){
                string += "*"
            }
            string += "\n"
        }
        console.log(string)
   }

    const hollowtriangle = () => {
        // *
        // **
        // * *
        // *  *
        // *   *
        // ******

        let n = 6
        let string = ""

        for (i=1; i<=n; i++){
            for (j=0; j<i; j++) {
                if(i == n) {
                    string += "*"
                } else {
                    if(j==0 || j== i-1){
                        string += "*"
                    } else {
                        string += " "
                    }
                }
            }
            string += "\n"
        }
        console.log(string)
    }

    const pyramid = () => {
        //      *
        //     ***
        //    *****
        //   *******
        //  *********
        let n = 5;
        let string = ""

        for (i=1; i<=n; i++){
            for(j=0; j < n -i; j++) {
                string += " "
            }

            for(k=0;k < 2 * i - 1; k++){
                string += "*"
            }

            string += "\n"
        }
        console.log(string)
    }

    const reversepyramid = () => {
    //    *********
    //     *******
    //      *****
    //       ***
    //        *
        let n = 5;
        let string = ""

        for (i=0; i<=n; i++){
            for(j=0; j < i; j++) {
                string += " "
            }

            for(k=0;k < 2 * (n - i) - 1; k++){
                string += "*"
            }

            string += "\n"
        }
        console.log(string)
    }

    const hollowpyramid = () => {
    //      *
    //     * *
    //    *   *
    //   *     *
    //  *********

        let n = 5 
        let string = ""

        for(i=0; i<=n;i++){
            for (let j = 1; j <= n-i; j++) {
                string += " ";
            }
            
            for(k=0; k < 2 * i - 1; k++){
                if(i==1 || i==n){
                    string += "*" 
                } else {
                    if(k === 0 || k === 2 * i-2) {
                        string += "*";
                    } else {
                        string += " " 
                    }
                }
            }
            string += "\n"
        }

        console.log(string)
    }

    const diamond = () => {
    //      *
    //     ***
    //    *****
    //   *******
    //  *********
    //   *******
    //    *****
    //     ***
    //      *
        let n = 5
        let string = ""

        //upward
        for(let i=1; i<= n; i++){
            for (let j = n; j > i; j--) {
                string += " ";
            }
            for (let k = 0; k < i * 2-1;k++) {
                string += "*";
            }
            string += "\n"
        }
         //downward
        for(i=1; i<= n-1; i++){
            for (let j = 0; j < i; j++) {
                string += " ";
            }
            for (let k =(n-i) * 2-1; k>0; k--) {
                string += "*";
            }
            string += "\n"
        }

        console.log(string)
    }

    const hollowdiamond = () => {
        //      *
        //     * *
        //    *   *
        //   *     *
        //  *       *
        //   *     *
        //    *   *
        //     * *
        //      *
        let n = 5;
        let string = ""
        //upside
        for (let i=1; i<= n; i++) {
           
            for (let j = n; j > i; j--) {
              string += " ";
            }
            
            for (let k=0; k< i * 2 - 1; k++) {
              if (k == 0 || k == 2 * i - 2) {
                string += "*";
              }
              else {
                string += " ";
              }
            }
            string += "\n";
        }

        //downside 
        for (let i=1; i<=n-1; i++) {
           
            for (let j=0; j<i; j++) {
                string += " ";
            }
           
            for (let k =(n-i)*2 - 1; k>=1; k--) {
                if (k ==1 || k == (n - i) * 2 - 1) {
                    string += "*";
                }
                else {
                    string += " ";
                }
            }
            string += "\n";
        }
        console.log(string)
    }

    const hourglass = () => {
        let n=5;
        let string = ""

        for (let i=0; i<n; i++){
            for(j=0; j < i; j++) {
                string += " "
            }

            for(let k=0;k < 2 * (n - i) - 1; k++){
                string += "*"
            }

            string += "\n"
        }

        for (let i=2; i<=n; i++){
            for(j=0; j < n -i; j++) {
                string += " "
            }

            for(k=0;k < 2 * i - 1; k++){
                string += "*"
            }

            string += "\n"
        }
        console.log(string)
    }

    const rightpascal = () => {
        // *
        // **
        // ***
        // ****
        // *****
        // ****
        // ***
        // **
        // *
        let n = 5
        let string = ""

       
        for(let i=1; i<=n; i++) {
            for (let j=0;j< i;j++) {
                string += "*"
            }
            string += "\n"
        }

        for(let i=1; i<=n; i++){
            for (let j=0; j < n-i; j++){
                //console.log(j,n)
                string += "*"
            }
            string += "\n"
        }


        console.log(string)
    }

    const leftpascal = () => {
        // *
        // **
        // ***
        // ****
        // *****
        // ****
        // ***
        // **
        // *
        let n = 5
        let string = ""

        for (let i = 1; i <= n; i++) {
            for (let j=0; j< n - i; j++) {
              string += " ";
            }

            for (let k =0; k<i; k++) {
              string += "*";
            }
            string += "\n";
        }

          for (let i= 1; i <= n-1; i++) {
            for (let j= 0; j < i; j++) {
              string += " ";
            }
            for (let k=0; k < n-i; k++) {
              string += "*";
            }
            string += "\n";
          }
        console.log(string);
    }

    const heart = () => {
        //  ***   ***
        // ***** *****
        // ***********
        //  *********
        //   *******
        //    *****
        //     ***
        //      *
        let n = 6;
        let string = "";
        // upper part
        for (let i= n/2; i<n; i+=2) {
           
            for (let j=1; j < n-i; j+=2) {
                string += " ";
            }

            for (let j=1; j< i+1; j++) {
                string += "*";
            }

            for (let j=1; j < n-i + 1;j++) {
                string += " ";
            }
              
            for (let j=1; j < i+ 1;j++) {
                string += "*";
            }
            string += "\n";
        }

        //lower
        for (let i=n;i > 0; i--) {
            for (let j=0;j < n-i;j++) {
                string += " ";
            }
            for (let j=1;j< i*2;j++) {
                string += "*";
            }
            string += "\n";
        }
        console.log(string);
    }
    
    squareStar()
    hollowSquare()
    righttriangle()
    lefttriandle()
    downtriangle()
    hollowtriangle()
    pyramid()
    reversepyramid()
    hollowpyramid()
    diamond()
    hollowdiamond()
    hourglass()
    rightpascal()
    leftpascal()
    heart()
})

