const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();

//let dev_db_url = 'mongodb+srv://aallesha:' + encodeURIComponent("aalisha287#") + '@cluster0.mpeft.mongodb.net/productstutorial?retryWrites=true&w=majority';

let mongoDB = process.env.DB_CONNECTION;

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

const product = require('./routes/product.route'); // Imports routes for the products
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/products', product);

let port = process.env.PORT;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});

