const Product = require('../models/product.model');

exports.test = function(req, res) {
    res.send('Greetings from test controller')
    //res.status(400).json({error: 'err'});
}

exports.product_create = function(req, res) {
    let product = new Product(
        {
            name: req.body.name,
            price: req.body.price
        }
    )

    product.save(function(err){
        if(err) {
            //return (err)
            res.status(401).json({error: err});
        }
        //res.send('Product created successfully')
        res.status(200).json({message: 'Product created successfully'});
    })
}

exports.product_details = function(req, res) {
    Product.findById(req.params.id, function(err, product){
        if(err){
            res.status(401).json({error: err});
        }
        //res.send(product)
        res.status(200).json({data: product});
    })
}

exports.product_update = function(req, res) {
    Product.findByIdAndUpdate(req.params.id, {$set: req.body}, function(err, product){
        if(err) {
            res.status(401).json({error: err});
        }
        //res.send('Product updated successfully')
        res.status(200).json({message: 'Product updated successfully'});
    })
}

exports.product_delete = function(req, res) {
    Product.findByIdAndRemove(req.params.id, function(err){
        if(err) {
            res.status(401).json({error: err});
        }
        //res.send('Deleted successfully')
        res.status(200).json({message: 'Deleted successfully'});
    })
}

exports.product_list = function(req, res) {
    Product.find({}, function(err, products){
        if(err) {
            res.status(401).json({error: err});
        }
        res.status(200).json({data: products});
    })
}


