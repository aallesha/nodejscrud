// const {MongoClient} = require('mongodb');
// 

// const url = process.env.DB_CONNECTION;
// const databaseName = process.env.DB_NAME;
// const client= new MongoClient(url);

// async function dbConnect()
// {
//     let result = await client.connect();
//     db= result.db(databaseName);
//     return db.collection('products');
  
// }
// module.exports = dbConnect;


const MongoClient = require("mongodb").MongoClient
require('dotenv').config();
const urlMongo = process.env.DB_CONNECTION;
const databaseName = process.env.DB_NAME;

var db;

function connectToServer( callback ) {
    MongoClient.connect(urlMongo,  { }, function( err, client ) {
        db = client.db(databaseName);
        return callback( err );
    })
}

function getDb() {
    return db
}

module.exports = {connectToServer, getDb}