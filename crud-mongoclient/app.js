const mongoose = require('mongoose');
const express = require('express');
const mongo = require('./dbconnect.js');
const bodyParser = require('body-parser');

const app = express();
require('dotenv').config();

    
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
console.log('1')
mongo.connectToServer(function( err) {
    console.log('2')
    if (err) console.log(err);
   
    let port = process.env.PORT;
    console.log('3')
    app.listen(port, () => {
        console.log('4')
        const product = require('./routes/product.route'); // Imports routes for the products
        app.use('/products', product);
        console.log('Server is up and running on port numner ' + port);
    });
});

