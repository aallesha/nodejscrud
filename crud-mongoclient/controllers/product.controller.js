//const Product = require('../models/product.model');
//const dbConnect = require('../dbconnect');
var ObjectId = require('mongodb').ObjectId
const db = require('../dbconnect.js').getDb()
const productsCollection = db.collection('products')

exports.test = function(req, res) {
    res.send('from test controller')
}

exports.product_create = async (req, res) => {
    let name = req.body.name
    let price = req.body.price
        
    try {
        const result = await productsCollection.insertOne({"name": name, "price": price});
        res.status(200).json({message: 'Product created successfully'});
    } catch (e) {
        res.status(401).json({error: e});
    } 
}


exports.product_details = async (req, res) => {
    try {
      
       const result = await productsCollection.findOne({_id: ObjectId(req.params.id)});
       res.status(200).json({data: result});
    } catch (e) {
        res.status(401).json({error: e});
    } 
    
}

exports.product_update = async (req, res) => {
    try {
        let result = await productsCollection.update(
            { _id: ObjectId(req.params.id)},
            {
                $set: req.body
            }
        )
       res.status(200).json({message: 'Product updated successfully'});
    } catch (e) {
        res.status(401).json({error: e});
    } 
}

exports.product_delete = async (req, res) => {
    try {
        await productsCollection.deleteMany({_id: ObjectId(req.params.id)})
        res.status(200).json({message: 'Deleted successfully'});
    } catch (e) {
        res.status(401).json({error: e});
    } 
}

exports.product_list = async (req, res) => {
    try {
       const result = await productsCollection.find().toArray();
       console.log()
       res.status(200).json({data: result});
    } catch (e) {
        res.status(401).json({error: e});
    } 
}


