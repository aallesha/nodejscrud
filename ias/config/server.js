import express from "express";
import bodyParser from "body-parser";
import multer from 'multer'
import userRoutes from '../src/routes/user.route.js';
import userServiceDetailRoutes from '../src/routes/user_service_detail.route.js';
import familyDetailRoutes from '../src/routes/family_detail.route.js'
import userDetailRoutes from '../src/routes/user_detail.route.js'
import miscRoutes from '../src/routes/misc.route.js'
import serviceRequestRoutes from '../src/routes/service_request.route.js'


const server = express();

server.use(bodyParser.json());

// for parsing application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: true }))

// for parsing multipart/form-data
//const upload = multer()
//server.use(upload.array()); 
//server.use(express.static('public'));

//routes
server.use(userRoutes)
server.use(userServiceDetailRoutes)
server.use(familyDetailRoutes)
server.use(userDetailRoutes)
server.use(miscRoutes)
server.use(serviceRequestRoutes)

export default server;