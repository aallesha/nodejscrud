
import dotenv from 'dotenv';
dotenv.config()

import mongoose from "mongoose";

class Connection {
    constructor() {
        // const url = process.env.DB_CONNECTION || `mongodb+srv://aallesha:aalisha287%23@cluster0.mpeft.mongodb.net/ias?retryWrites=true&w=majority`;
        const url = `mongodb+srv://aallesha:aalisha287%23@cluster0.mpeft.mongodb.net/ias?retryWrites=true&w=majority`;
        mongoose.connect(url);
        mongoose.Promise = global.Promise;

        let db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));
    }
}

export default new Connection();