import dotenv from 'dotenv';
dotenv.config()

import "./config/database.js";
import server from "./config/server.js";

const port = process.env.PORT || 5000;

server.listen(port, () => {
  console.log(`app running on port ${port}`);
});