import dotenv from 'dotenv';
dotenv.config() 

import mongoose from 'mongoose'
import UserServiceDetail from '../models/user_service_detail.model.js'
import { validateServiceDetails } from '../validations/user_service_detail.js'

class UserServiceDetailController {
    addServiceDetails = async(req, res) => {

        let errors = validateServiceDetails(req.body);
        
        //check if input data is valid
        if (Object.keys(errors).length === 0) {
            try {
              
                //collect data
                let detail = {
                    user_id: req.user.user_id,
                    designation: req.body.designation,
                    department_id: req.body.department_id,
                    is_additional_charge: req.body.is_additional_charge,
                    last_serving_department: req.body.last_serving_department,
                    service_status: req.body.service_status,
                    start_date: req.body.start_date,
                    till_date: req.body.end_date,
                    address: req.body.address,
                    district_id: req.body.district_id,
                    other_details: req.body.other_details,
                }

                //save details
                UserServiceDetail.create(detail, (err, result) => {
                    if (err) {
                        res.status(400).json({ status: false, message: err, data: [] })
                    } else {
                        res.status(200).json({ status: true, message: "Service details added successfully", data: result })
                    }
                })

            } catch (e) {
                res.status(400).json({ status: false, message: 'Some error occured', data: [] })
            }

        } else {
            res.status(400).json({message: errors, data: [], status: false});
        }
    }

    getAllServices = async(req, res) => {
        try {
            UserServiceDetail.find({}, (err, result) => {
                if (err) {
                    res.status(400).json({status: false, message: err, data: []})
                }
                else {
                    res.status(200).json({status: true, message: "Success", data: result})
                }
            });
        } catch (e) {
            res.status(400).json({ status: false, message: 'Some error occured', data: []})
        }
    }

    getServiceDetail = async(req, res) => {
        try {
            let errors = {};

            if (!req.params.id) {
                errors.id = "User Service id is required"
            }
        
            //check if input data is valid
            if (Object.keys(errors).length === 0) { 

                UserServiceDetail.findById(req.params.id, function (err, result) {
                    if (err) {
                        res.status(400).json({ status: false, message: err, data: []})
                    } else {
                        res.status(200).json({ status: true, message: "Success", data: result })
                    }
                });
            } else {
                res.status(400).json({ status: false, message: errors, data: []})
            }
            
        } catch (e) {
            res.status(400).json({ status: false, message: 'Some error occured', data: []})
        }
    }

    updateServiceDetail = async(req, res) => {
        try {

            let errors = validateServiceDetails(req.body);

            if (!req.params.id) {
                errors.id = "User Service id is required"
            }
        
            //check if input data is valid
            if (Object.keys(errors).length === 0) { 
               
                //collect data
                let detail = {
                    user_id: req.user.user_id,
                    designation: req.body.designation,
                    department_id: req.body.department_id,
                    is_additional_charge: req.body.is_additional_charge,
                    last_serving_department: req.body.last_serving_department,
                    service_status: req.body.service_status,
                    start_date: req.body.start_date,
                    till_date: req.body.end_date,
                    address: req.body.address,
                    district_id: req.body.district_id,
                    other_details: req.body.other_details,
                }

                UserServiceDetail.findByIdAndUpdate(req.params.id, detail, {new: true}, function (err, result) {
                    if (err) {
                        res.status(400).json({ status: false, message: err, data: []})
                    } else {
                        res.status(200).json({ status: true, message: "Success", data: result })
                    }
                });
            } else {
                res.status(400).json({ status: false, message: errors, data: []})
            }
        } catch (e) {
            res.status(400).json({ status: false, message: 'Some error occured', data: []})
        }
    }

    deleteServiceDetail = async(req, res) => {
        try {
            let errors = {};

            if (!req.params.id) {
                errors.id = "User Service id is required"
            }
        
            //check if input data is valid
            if (Object.keys(errors).length === 0) { 

                const id = mongoose.Types.ObjectId(req.params.id);

                UserServiceDetail.findOneAndDelete({ _id: id }, function (err, result) {
                    if (err) {
                        res.status(500).json({ status: false, message: err })
                    } else {
                        res.status(200).json({ status: true, message: "Deleted successfully", data: [] })
                    }
                });

            } else {
                res.status(400).json({ status: false, message: errors, data: []})
            }
        } catch (e) {
            res.status(400).json({ status: false, message: 'Some error occured', data: []})
        }
    }
} 

export default new UserServiceDetailController()