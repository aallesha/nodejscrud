import FamilyDetail from '../models/family_detail.model.js'
import {validateAddFamilyDetail} from '../validations/family_detail.js'

class FamilyDetailController {
    addFamilyDetail = async(req, res) => {
        let errors = validateAddFamilyDetail(req.body);
        
        //check if input data is valid
        if (Object.keys(errors).length === 0) {
            try {

                req.body.user_id = req.user.user_id

                //save family details
                FamilyDetail.create(req.body, (err, result) => {
                    if (err) {
                        res.status(400).json({ status: false, message: err, data:[] })
                    } else {
                        res.status(200).json({ status: true, message: "Family details added successfully", data: result })
                    }
                })

            } catch(e) {
                res.status(400).json({ status: false, message: 'Some error occured', data: [] })
            }
        } else {
            res.status(400).json({ status: false, message: errors, data: []})
        }
    }

    updateFamilyDetail = async(req, res) => {

        try {
            let errors = validateAddFamilyDetail(req.body);
            
            if (!req.params.id) {
                errors.id = "Family Detail id is required"
            }

            if(Object.keys(errors).length == 0) {

                req.body.user_id = req.user.user_id
                
                //update family details
                FamilyDetail.findByIdAndUpdate(req.params.id, req.body, {new: true}, (err, result) => {
                    if (err) {
                        res.status(400).json({ status: false, message: err, data:[] })
                    } else {
                        res.status(200).json({ status: true, message: "Family details updated successfully", data: result })
                    }
                })
            } else {
                res.status(400).json({ status: false, message: errors, data: []})
            }

        } catch(e) {
            res.status(400).json({ status: false, message: 'Some error occured', data: [] })
        }
    }

    getFamilyDetail = async(req, res) => {
        try {

            let errors = {}

            if(!req.params.id) {
                errors.id = 'Family Detail id is required'
            }

            if(Object.keys(errors).length == 0) {

                FamilyDetail.findById(req.params.id, (err, result) => {
                    if (err) {
                        res.status(400).json({ status: false, message: err, data:[]})
                    } else {
                        res.status(200).json({ status: true, message: "Success", data: result })
                    }
                })
            } else {
                res.status(400).json({ status: false, message: errors, data: []})
            }

        } catch(e) {
            res.status(400).json({ status: false, message: 'Some error occured', data: []})
        }
    }

    getFamilyDetailList = async(req, res) => {
        try {

            FamilyDetail.find({user_id: req.user.user_id}, (err, result) => {
                if (err) {
                    res.status(400).json({ status: false, message: err, data:[]})
                } else {
                    res.status(200).json({ status: true, message: "Success", data: result })
                }
            })
            
        } catch(e) {
            res.status(400).json({ status: false, message: 'Some error occured', data: []})
        }
    }

    deleteFamilyDetail = async(req, res) => {
        try {
            
            let errors = {}

            if(!req.params.id){
                errors.id = 'Family Detail id is required'
            }

            FamilyDetail.deleteOne({id: req.params.id}, (err, result) => {
                if (err) {
                    res.status(400).json({ status: false, message: err, data:[]})
                } else {
                    res.status(200).json({ status: true, message: "Deleted successfully", data: [] })
                }
            })
            
        } catch(e) {
            res.status(400).json({ status: false, message: 'Some error occured', data: []})
        }
    }
}

export default new FamilyDetailController()