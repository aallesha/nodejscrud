import District from '../models/district.model.js'
import Gender from '../models/gender.model.js'
import Department from '../models/department.model.js'
import CurrentStatus from '../models/current_status.model.js'

class MiscController {
    getDistrict = async(req, res) => {
        try {
            District.find({}, (err, result) => {
                if(err) {
                    res.status(400).json({status: false, message: err, data:[]})
                } else {
                    res.status(200).json({status: true, message: "Success", data: result})
                }
            })
        } catch(e) {
            res.status(400).json({status: false, message: 'Some error occured', data: []})
        }
    }

    getGender = async(req, res) => {
        try {
            Gender.find({}, (err, result) => {
                if(err) {
                    res.status(400).json({status: false, message: err, data:[]})
                } else {
                    res.status(200).json({status: true, message: "Success", data: result})
                }
            })
        } catch(e) {
            res.status(400).json({status: false, message: 'Some error occured', data: []})
        }
    }

    getDepartment = async(req, res) => {
        try {
            Department.find({}, (err, result) => {
                if(err) {
                    res.status(400).json({status: false, message: err, data:[]})
                } else {
                    res.status(200).json({status: true, message: "Success", data: result})
                }
            })
        } catch(e) {
            res.status(400).json({status: false, message: 'Some error occured', data: []})
        }
    }

    getCurrentStatus = async(req, res) => {
        try {
            CurrentStatus.find({}, (err, result) => {
                if(err) {
                    res.status(400).json({status: false, message: err, data:[]})
                } else {
                    res.status(200).json({status: true, message: "Success", data: result})
                }
            })
        } catch(e) {
            res.status(400).json({status: false, message: 'Some error occured', data: []})
        }
    }
}

export default new MiscController()