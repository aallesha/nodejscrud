import ServiceRequest from '../models/service_request.model.js'
import ServiceRequestDocument from '../models/service_request_document.model.js'
import {validateAddServiceRequest} from '../validations/service_request.js'


class ServiceRequestController {
    addServiceRequest = (req, res) => {
       
        try {

            let errors = validateAddServiceRequest(req.body);

            if(Object.keys(errors).length === 0) {
                req.body.user_id = req.user.user_id

                ServiceRequest.create(req.body, (err, result) => {
                    if(err) {
                        res.status(400).json({status: false, message: err.message, data:[]})
                    } else {
                        if (req.files.length > 0) {
                            for (let i = 0; i < req.files.length; i++) {
                                ServiceRequestDocument.create({name: req.files[i].filename, service_requests_id: result._id});
                            }
                        }

                        res.status(200).json({status: true, message: 'Service request created successfully', data:result})
                    }
                })
            } else {
                res.status(400).json({status: false, message: errors, data:[]})
            }
        } catch(e) {
            res.status(400).json({status: false, message: e.message, data: []})
        }
    }

    getServiceRequestList = (req, res) => {
        try {
           
            ServiceRequest.find({user_id: req.user.user_id}).populate('attachments')
            .then(result => {
                res.status(200).json({status: true, message: 'Success', data: result})
            })
            .catch(err => {
                res.status(400).json({status: false, message: err.message, data: []})
            });

        } catch (e) {
            res.status(400).json({status: false, message: e.message, data: []})
        }
    }

    getServiceRequest = (req, res) => {
        try {
            let errors = {}

            if(!req.params.id){
                errors.id = 'Service Request id is required'
            }

            if(Object.keys(errors).length === 0){

                ServiceRequest.findById(req.params.id).populate('attachments')
                .then(result => {
                    res.status(200).json({status: true, message: 'Success', data: result})
                })
                .catch(err => {
                    res.status(400).json({status: false, message: err.message, data: []})
                });

            } else {
                res.status(400).json({status: false, message: errors, data: []})
            }
        } catch (e) {
            res.status(400).json({status: false, message: e.message, data: []})
        }
    }

    deleteServiceRequest = (req, res) => {
        try {
            
            let errors = {}

            if(!req.params.id){
                errors.id = 'Service Request id is required'
            }


            ServiceRequest.findById(req.params.id, function(err, service_request){
                service_request.remove(function(err){
                    if(!err) {
                        //ServiceRequestDocument.find({service_requests_id: req.params.id}).remove()
                        ServiceRequestDocument.deleteMany({ service_requests_id: req.params.id}, function(err, result) {
                            if(!err) {
                                res.status(200).json({ status: true, message: "Deleted successfully", data: [] })   
                            } else {
                                res.status(400).json({status: false, message: err, data: []}) 
                            }
                        })         
                              
                    } else {
                        res.status(400).json({status: false, message: err, data: []})                                
                    }
                });
            });
                
            
        } catch(e) {
            res.status(400).json({ status: false, message: e.message, data: []})
        }
    }
}

export default new ServiceRequestController()