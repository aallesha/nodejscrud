import express from 'express'
import multer from 'multer'
import auth from '../middleware/auth.js'
import ServiceRequestController from '../controllers/service_request.controller.js'

const router = express.Router()

// const upload = multer({
//     storage: multerStorage,
//     fileFilter: multerFilter,
// })

const multerStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/service_request_documents/')
    },
    filename: (req, file, cb) => {
        console.log(file.originalname)
        cb(null, `${Date.now()}-${file.originalname}`)
    },
})

const upload = multer({
    storage: multerStorage
})

router.post('/api/service-request', upload.array('attachment', 5), auth, ServiceRequestController.addServiceRequest)
router.get('/api/service-request', auth, ServiceRequestController.getServiceRequestList)
router.get('/api/service-request/:id', auth, ServiceRequestController.getServiceRequest)
router.delete('/api/service-request/:id', auth, ServiceRequestController.deleteServiceRequest)

export default router