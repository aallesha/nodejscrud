import express from 'express';
import UserController from '../controllers/user.controller.js';
const router = express.Router();

router.post('/api/register', UserController.register)
router.post('/api/login', UserController.login)
router.post('/api/otp-verify', UserController.verifyOTP)
router.post('/api/resend-otp', UserController.resendOTP)
router.post('/api/reset-password/send-otp', UserController.forgetPasswordOTP)
router.put('/api/reset-password', UserController.resetPassword)

export default router