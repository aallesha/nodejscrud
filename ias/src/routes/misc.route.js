import express from 'express'
const router = express.Router()

import auth from '../middleware/auth.js'
import MiscController from '../controllers/misc.controller.js'

router.get('/api/district', MiscController.getDistrict)
router.get('/api/gender', MiscController.getGender)
router.get('/api/department', MiscController.getDepartment)
router.get('/api/current-status', MiscController.getCurrentStatus)

export default router