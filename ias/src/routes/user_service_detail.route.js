import express from 'express';
import UserServiceDetailController from '../controllers/user_service_detail.controller.js';
const router = express.Router();
import auth from '../middleware/auth.js'

router.post('/api/service-details', auth, UserServiceDetailController.addServiceDetails)
router.get('/api/service-details', auth, UserServiceDetailController.getAllServices)
router.get('/api/service-details/:id', auth, UserServiceDetailController.getServiceDetail)
router.patch('/api/service-details/:id', auth, UserServiceDetailController.updateServiceDetail)
router.delete('/api/service-details/:id', auth, UserServiceDetailController.deleteServiceDetail)

export default router