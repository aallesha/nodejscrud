import express from 'express'
const router = express.Router()

import auth from '../middleware/auth.js'
import FamilyDetailController from '../controllers/family_detail.controller.js'

router.post('/api/family-details', auth, FamilyDetailController.addFamilyDetail)
router.patch('/api/family-details/:id', auth, FamilyDetailController.updateFamilyDetail)
router.get('/api/family-details/:id', auth, FamilyDetailController.getFamilyDetail)
router.get('/api/family-details', auth, FamilyDetailController.getFamilyDetailList)
router.delete('/api/family-details/:id', auth, FamilyDetailController.deleteFamilyDetail)

export default router

