import mongoose from 'mongoose'

const UserServiceDetailSchema = new mongoose.Schema(
    {
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User',
        },
        designation: {
            type: String,
            required: true,
            maxLength: 50
        },
        department_id:
        {
            type: Number,
            required: true,
        },
        is_additional_charge:
        {
            type: Number,
            required: true,
        },
        last_serving_department:
        {
            type: String,
        },
        service_status:
        {
            type: Number,
            required: true,
        },
        start_date:
        {
            type: Date,
            required: true,
        },
        till_date:
        {
            type: Date
        },
        address:
        {
            type: String,
            required: true,
            maxLength: 255
        },
        district_id:
        {
            type: Number,
            required: true,
        },
        other_details:
        {
            type: String,
            maxLength: 255
        },
    }, 
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    }
)

const UserServiceDetail = new mongoose.model('UserServiceDetail', UserServiceDetailSchema)

export default UserServiceDetail