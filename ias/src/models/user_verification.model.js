import mongoose from 'mongoose'

const UserVerificationSchema = new mongoose.Schema(
    {
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'User',
        },
        verification_code: {
            type: String,
            required: true
        },
        for: {
            type: Number,
            required: true
        },
        expire: {
            type: Date,
            required: true
        }
    }, 
    {
        timestamps: true
    }
)

const UserVerification = mongoose.model('UserVerification', UserVerificationSchema)

export { UserVerification, UserVerificationSchema }