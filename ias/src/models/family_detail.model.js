import mongooose from 'mongoose'

const FamilyDetailSchema = new mongooose.Schema(
    {
        user_id: {
            type: mongooose.Schema.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        fname: {
            type: String,
            required: true
        },
        middle_name: {
            type: String,
        },
        last_name: {
            type: String,
        },
        age: {
            type: String,
        },
        relation: {
            type: String,
            required: true
        },
        gender: {
            type: String,
            required: true
        },
        mobile_no: {
            type: String,
        },
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
    }
)

const FamilyDetail = mongooose.model('FamilyDetail', FamilyDetailSchema)

export default FamilyDetail