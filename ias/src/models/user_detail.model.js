import mongoose from 'mongoose'

const UserDetailSchema = new mongoose.Schema(
    {
        user_id: {
            type: mongoose.Types.ObjectId,
            required: true,
            ref: 'User'
        },
        first_name: {
            type: String,
            required: true,
        },
        middle_name: {
            type: String,
        },
        last_name: {
            type: String,
        },
        dob: {
            type: Date
        },
        gender_id: {
            type: Number
        },
        current_status: {
            type: Number
        },
        batch: {
            type: Number 
        },
        is_address_same: {
            type: Number,
            default: 0
        },
        temporary_address: {
            type: String
        },
        temporary_district: {
            type: Number
        },
        permanent_address: {
            type: String
        },
        permanent_district: {
            type: Number
        },
    }
)

const UserDetail = new mongoose.model('UserDetail', UserDetailSchema)

export default UserDetail