import mongoose from 'mongoose'

const departmentSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        }
    },
    {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}}
)

const Department = new mongoose.model('Department', departmentSchema)

export default Department