import mongoose from 'mongoose'

const districtSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        }
    },
    {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}}
)

const District = new mongoose.model('District', districtSchema)

export default District