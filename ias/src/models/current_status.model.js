import mongoose from 'mongoose'

const currentStatusSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        }
    },
    {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}}
)

const CurrentStatus = new mongoose.model('CurrentStatus', currentStatusSchema)

export default CurrentStatus