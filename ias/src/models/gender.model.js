import mongoose from 'mongoose'

const genderSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true
        }
    },
    {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}}
)

const Gender = new mongoose.model('Gender', genderSchema)

export default Gender