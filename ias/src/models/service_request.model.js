import mongoose from 'mongoose'
const {Schema} = mongoose

const serviceRequestSchema = new mongoose.Schema(
    {
        user_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            required: true
        },
        name: {
            type: String,
            required: true,
            max: 100,
        },
        designation_name: {
            type: String,
            required: true
        },
        current_status: {
            type: Number,
            required: true
        },
        district_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'District',
            required: true
        },
        mobile: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        request_priority: {
            type: Number,
            required: true
        },
        request_date: {
            type: Date
        },
        due_date: {
            type: Date
        },
        subject: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        office_district_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'District',
            required: true
        },
        department_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Department',
            required: true
        },
        application_status: {
            type: Number,
            required: true
        },
        is_draft: {
            type: Number,
            required: true,
            default: 0
        },
        // attachments: [{
        //     type: Schema.Types.ObjectId,
        //     ref: 'ServiceRequestDocument'
        // }]
    },
    {
        timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}
    }
)

serviceRequestSchema.virtual('attachments', {
    ref: 'ServiceRequestDocument', //The Model to use
    localField: '_id', //Find in Model, where localField 
    foreignField: 'service_requests_id', // is equal to foreignField
 });
 
 // Set Object and Json property to true. Default is set to false
 serviceRequestSchema.set('toObject', { virtuals: true });
 serviceRequestSchema.set('toJSON', { virtuals: true });

export default new mongoose.model('ServiceRequest', serviceRequestSchema)