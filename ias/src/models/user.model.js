import mongoose from 'mongoose'
import jwt from 'jsonwebtoken';

const UserSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        mobile: {
            type: Number,
            required: true,
        },
        email_verified_at: {
            type: Date,
        },
        password: {
            type: String
        },
        is_approve: {
            type: Boolean
        },
        remember_token: {
            type: String
        }
    }, 
    { 
        timestamps: true 
    }
)

UserSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({user_id: this._id, email: this.email }, process.env.TOKEN_KEY, 
        //{expiresIn: "4h"}
    );
    return token;
};

UserSchema.virtual('userverification', {
    ref: 'UserVerification',
    localField: '_id', // Of current collection
    foreignField: 'user_id',    // Of user collection
    justOne: true
})

UserSchema.set('toObject', { virtuals: true });
UserSchema.set('toJSON', { virtuals: true });

const User = mongoose.model("User", UserSchema)

export default User;