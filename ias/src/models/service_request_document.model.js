import mongoose from 'mongoose'
const {Schema} = mongoose

const serviceRequestDocSchema = new mongoose.Schema(
    {
        service_requests_id: {
            type: Schema.Types.ObjectId,
            ref: 'ServiceRequest',
            required: true
        },
        name: {
            type: String,
            required: true
        }
    },
    {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}}
)

const ServiceRequestDocument = new mongoose.model('ServiceRequestDocument', serviceRequestDocSchema)

export default ServiceRequestDocument