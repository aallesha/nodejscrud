const validateUser = (data) => {
    let errors = {}
    
    if(!data.name) {
        errors.name = "Name is required"
    }

    if(!data.mobile) {
        errors.mobile = "Mobile No. is required"
    } else if (data.mobile.length != 10 && data.mobile) {
        errors.mobile = 'Invalid Mobile No.';
    }

    if(!data.email) {
        errors.email = "Email is required"
    } else if(!validateEmail(data.email)){
        errors.email = 'Invalid Email';
    }

    if(!data.password){
        errors.password = "Password is required"
    } else if ((data.password.length < 6 || data.password.length > 16) && data.password) {
        errors.mobile = 'Password must be between 6 to 16 characters';
    }

    return errors;
}

const validateEmail = (email) => {
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(email).toLowerCase());
}

const validateLoginDetails = (data) => {
    let errors = {}

    if(!data.email) {
        errors.email = "Email is required"
    } else if(!validateEmail(data.email)){
        errors.email = 'Invalid Email';
    }

    if(!data.password){
        errors.password = "Password is required"
    } 

    return errors;
}

const validateOTPDetails = (data) => {
    let errors = {}

    if(!data.email) {
        errors.email = "Email is required"
    } else if(!validateEmail(data.email)){
        errors.email = 'Invalid Email';
    }

    if(!data.verification_code){
        errors.verification_code = "OTP is required"
    } 

    return errors;
}

const validateResendOTPDetails = (data) => {
    let errors = {}

    if(!data.email) {
        errors.email = "Email is required"
    } else if(!validateEmail(data.email)){
        errors.email = 'Invalid Email';
    }

    return errors;
}

const validateResetPasswordDetails  = (data) => {
    let errors = {}

    if(!data.email) {
        errors.email = "Email is required"
    } else if(!validateEmail(data.email)){
        errors.email = 'Invalid Email';
    }

    if(!data.verification_code){
        errors.verification_code = "OTP is required"
    } 

    if(!data.password){
        errors.password = "Password is required"
    } 

    return errors;
}

export { validateUser, validateLoginDetails, validateOTPDetails, validateResendOTPDetails, validateResetPasswordDetails }