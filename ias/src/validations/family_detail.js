const validateAddFamilyDetail = (data) => {

    let errors = {}

    if(!data.fname) {
        errors.fname = "First name is required"
    }

    if(!data.relation) {
        errors.relation = "Relationship is required"
    }

    if(!data.gender) {
        errors.gender = "Gender is required"
    }

    return errors
}

export { validateAddFamilyDetail }