const validateAddServiceRequest = (data) => {
    let errors = {}

    if(!data.name) {
        errors.name = 'Name is required'
    }

    if(!data.designation_name) {
        errors.designation_name = 'Designation is required'
    }

    if(!data.current_status) {
        errors.current_status = 'Current Status is required'
    }

    if(!data.district_id) {
        errors.district_id = 'District Id is required'
    }

    if(!data.mobile) {
        errors.mobile = 'Mobile no. is required'
    }

    if(!data.email) {
        errors.email = 'Email is required'
    }

    if(!data.request_priority) {
        errors.request_priority = 'Request Priority is required'
    }

    if(data.request_date){
        if(!new Date(data.request_date) instanceof Date) {
            errors.request_date = 'Request Date format is not proper'
        }
    }

    // if(data.due_date){
    //     if(new Date(data.due_date) instanceof Date && !isNaN(new Date(data.request_date).valueOf())) {
    //         errors.due_date = 'Due Date format is not proper'
    //     }
    // }

    if(data.due_date){
        if(!new Date(data.due_date) instanceof Date) {
            errors.due_date = 'Due Date format is not proper'
        }
    }

    if(!data.subject) {
       errors.subject = 'Subject is required' 
    }

    if(!data.description) {
        errors.description = 'Description is required' 
    }

    if(!data.application_status) {
        errors.application_status = 'Application Status is required'
    }

    return errors
}

export {validateAddServiceRequest}