const validateAddUserDetail = (data) => {
    let errors = {}

    if(!data.first_name){
        errors.first_name = "First Name is required"
    }

    return errors
}

export { validateAddUserDetail }