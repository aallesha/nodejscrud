const validateServiceDetails = (data) => {
    let errors = {}

    if (!data.designation) {
        errors.designation = "Designation is required"
    }

    if (!data.department_id) {
        errors.department_id = "Department is required"
    }

    if (!data.is_additional_charge) {
        errors.is_additional_charge = "Additional Charge Status is required"
    }

    if (!data.service_status) {
        errors.serving_status = "Service Status is required"
    }

    if (!data.start_date) {
        errors.start_date = "Select Start Date"
    }

    if (!data.address) {
        errors.address = "Address is required"
    }

    if (!data.district_id) {
        errors.district_id = "District is required"
    }

    return errors;
}

export { validateServiceDetails }