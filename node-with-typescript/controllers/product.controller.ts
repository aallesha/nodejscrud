
import Product from '../models/product.model' 
import { Request, Response } from "express";

export const product_create = (req: Request, res: Response) => {
    const product = new Product(req.body);
    product.save((err: any) => {
        if (err) {
            res.status(401).json({error: err});
        } else {
            res.status(200).json({message: 'Product created successfully'});
        }
    });
};

export const product_details = (req: Request, res: Response) => {
    Product.findById(req.params.id, function(err: any, product: any){
        if(err){
            res.status(401).json({error: err});
        }
        res.status(200).json({data: product});
    })
}

export const product_update = (req: Request, res: Response) => {
    Product.findByIdAndUpdate(req.params.id, {$set: req.body}, function(err: any, product: any){
        if(err) {
            res.status(401).json({error: err});
        }
        res.status(200).json({message: 'Product updated successfully'});
    })
}

export const product_delete = (req: Request, res: Response) => {
    Product.findByIdAndRemove(req.params.id, function(err: any){
        if(err) {
            res.status(401).json({error: err});
        }
        res.status(200).json({message: 'Deleted successfully'});
    })
}

export const product_list = (req: Request, res: Response) => {
    Product.find({}, function(err: any, products: any){
        if(err) {
            res.status(401).json({error: err});
        }
        res.status(200).json({data: products});
    })
}


