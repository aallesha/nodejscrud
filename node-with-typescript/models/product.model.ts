import mongoose, { Schema, model } from 'mongoose'

interface Product {
    name: string;
    price: number;
}

let ProductSchema = new Schema<Product>({
    name: {type: String, required: true},
    price: {type: Number, required: true}
})

export default mongoose.model<Product>('Product', ProductSchema)