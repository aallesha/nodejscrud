"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require('mongoose');
const express = require('express');
const product_route_1 = __importDefault(require("./routes/product.route"));
const bodyParser = require('body-parser');
require('dotenv').config();
const app = express();
let mongoDB = process.env.DB_CONNECTION;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/products', product_route_1.default);
let port = process.env.PORT;
app.listen(port, () => {
    console.log('Server is up and running on port ' + port);
});
//# sourceMappingURL=app.js.map