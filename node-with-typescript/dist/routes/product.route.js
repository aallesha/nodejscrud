"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require('express');
const router = express.Router();
const product_controller_1 = require("../controllers/product.controller");
router.post('/create', product_controller_1.product_create);
router.get('/:id', product_controller_1.product_details);
router.put('/:id/update', product_controller_1.product_update);
router.delete('/:id/delete', product_controller_1.product_delete);
router.get('/listing', product_controller_1.product_list);
exports.default = router;
//# sourceMappingURL=product.route.js.map