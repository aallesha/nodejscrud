"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.product_list = exports.product_delete = exports.product_update = exports.product_details = exports.product_create = void 0;
const product_model_1 = __importDefault(require("../models/product.model"));
const product_create = (req, res) => {
    const product = new product_model_1.default(req.body);
    product.save((err) => {
        if (err) {
            res.status(401).json({ error: err });
        }
        else {
            res.status(200).json({ message: 'Product created successfully' });
        }
    });
};
exports.product_create = product_create;
const product_details = (req, res) => {
    product_model_1.default.findById(req.params.id, function (err, product) {
        if (err) {
            res.status(401).json({ error: err });
        }
        res.status(200).json({ data: product });
    });
};
exports.product_details = product_details;
const product_update = (req, res) => {
    product_model_1.default.findByIdAndUpdate(req.params.id, { $set: req.body }, function (err, product) {
        if (err) {
            res.status(401).json({ error: err });
        }
        res.status(200).json({ message: 'Product updated successfully' });
    });
};
exports.product_update = product_update;
const product_delete = (req, res) => {
    product_model_1.default.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.status(401).json({ error: err });
        }
        res.status(200).json({ message: 'Deleted successfully' });
    });
};
exports.product_delete = product_delete;
const product_list = (req, res) => {
    product_model_1.default.find({}, function (err, products) {
        if (err) {
            res.status(401).json({ error: err });
        }
        res.status(200).json({ data: products });
    });
};
exports.product_list = product_list;
//# sourceMappingURL=product.controller.js.map