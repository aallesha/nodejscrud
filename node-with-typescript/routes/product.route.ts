const express = require('express')
const router = express.Router()
import { product_list, product_create, product_details, product_delete, product_update, } from '../controllers/product.controller'

router.post('/create', product_create);

router.get('/:id', product_details);

router.put('/:id/update', product_update);

router.delete('/:id/delete', product_delete);

router.get('/listing', product_list);

export default router