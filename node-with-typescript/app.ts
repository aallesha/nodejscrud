const mongoose = require('mongoose');
const express = require('express');
import router from './routes/product.route'
const bodyParser = require('body-parser');
require('dotenv').config();

const app = express();

let mongoDB = process.env.DB_CONNECTION;

mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use('/products', router);

let port = process.env.PORT;

app.listen(port, () => {
    console.log('Server is up and running on port ' + port);
});

