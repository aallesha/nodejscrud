const num1Input = document.getElementById('num1') as HTMLInputElement;
const num2Input = <HTMLInputElement> document.getElementById('num2');
const buttonElememt = document.querySelector('button');

//declaration of types
function add(a: number, b: number) {
    return a + b;
}

const result = add(5, 2)

console.log(result)

function printResult(result) {
    console.log(result)
}

//literal types

// function printResult1(result, mode: 'console' | 'alert') {

// use below for systematic code
type printmode = 'console' | 'alert'
function printResultNew(result, mode: printmode) {
    if(mode === 'console') {
        console.log(result)
    } else {
        alert(result)
    }
}


//let results: {res: number} [] = []

//let results: {res: number, print: () => void }[] = []

// simplified version of above
type CalculationResults = {res: number, print: () => void }[];
const results: CalculationResults = []

buttonElememt.addEventListener('click', () => {
    const num1 = +num1Input.value
    const num2 = +num2Input.value

    const result = add(num1, num2)

    // const resultContainer: {res: number} = {
    //     res: result,
    //     print() { doesn't work
    //         console.log(this.res)
    //     }
    // }

    const resultContainer = {
        res: result,
        print() {
            console.log(this.res)
        }
    }

    results.push(resultContainer)
    //results.push(5) // not assignable as its an object
    //printResult(resultContainer.res)
    printResult(results)
    results[0].print()

    printResult1(result, 'console')
    printResult1(result, 'alert')


    //printResult1(result, 'window') // will give error as its not defined in literal types
})

//enum
enum UserResponse {
    No = 0,
    Yes = 1,
}
   
function respond(recipient: string, message: UserResponse): void {
   
}
   
respond("John", UserResponse.Yes);


// classes

class User {
    //valid in typescript
    name:string;
    private age: number;

    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
    }
}

const user = new User('ABC', 30)
//console.log(user.name, user.age) // age is not valid as it is outside class and has private modifier
console.log(user.name)

// above code can be written as below also
class User1 {
    constructor(public name: string, private age: number){
        this.name = name; // also can remove this
        this.age = age;  // also can remove this
    }
}

class Admin extends User1 {
    constructor(name: string, age: number, private permissions: string[]) {
        super(name, age)
    }
}


//interfaces

interface Person {   
    name:string    
}  

interface Printable {
    print(): void
}

class PersonalDetail implements Person, Printable {   
    constructor(public name: string){

    }

    print() {
        console.log(this.name)
    }
}  


//generic types

function logAndEcho<T>(val: T) {
    console.log(val)
    return val
}

//logAndEcho('Hi, there').split('') // will give error as string type not defined
logAndEcho<string>('Hi, there').split('')


// interface User {
//     name: string;
//     age: number;
//     occupation: string;
//    }
    
//    interface Admin {
//     name: string;
//     age: number;
//     role: string;
//    }
    
//    export type Person = User | Admin;
    
//    export const persons: Person[] = [
//     {
//     name: 'Max Mustermann',
//     age: 25,
//     occupation: 'Chimney sweep'
//     },
//     {
//     name: 'Jane Doe',
//     age: 32,
//     role: 'Administrator'
//     },
//     {
//     name: 'Kate Müller',
//     age: 23,
//     occupation: 'Astronaut'
//     }, 
//     {
//     name: 'Bruce Willis',
//     age: 64,
//     role: 'World saver'
//     }
//    ];
    
//    export function logPerson(person: Person) {
//     let additionalInformation: string;

//     if ("role" in person) {
//         additionalInformation = person.role;
//     } else {
//         additionalInformation = person.occupation;
//     }
//     console.log(` - ${person.name}, ${person.age}, ${additionalInformation}`);
//    }
    
//    persons.forEach(logPerson);