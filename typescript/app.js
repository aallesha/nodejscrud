var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var num1Input = document.getElementById('num1');
var num2Input = document.getElementById('num2');
var buttonElememt = document.querySelector('button');
//declaration of types
function add(a, b) {
    return a + b;
}
var result = add(5, 2);
console.log(result);
function printResult(result) {
    console.log(result);
}
function printResult1(result, mode) {
    if (mode === 'console') {
        console.log(result);
    }
    else {
        alert(result);
    }
}
var results = [];
buttonElememt.addEventListener('click', function () {
    var num1 = +num1Input.value;
    var num2 = +num2Input.value;
    var result = add(num1, num2);
    // const resultContainer: {res: number} = {
    //     res: result,
    //     print() { doesn't work
    //         console.log(this.res)
    //     }
    // }
    var resultContainer = {
        res: result,
        print: function () {
            console.log(this.res);
        }
    };
    results.push(resultContainer);
    //results.push(5) // not assignable as its an object
    //printResult(resultContainer.res)
    printResult(results);
    results[0].print();
    printResult1(result, 'console');
    printResult1(result, 'alert');
    //printResult1(result, 'window') // will give error as its not defined in literal types
});
//enum
var UserResponse;
(function (UserResponse) {
    UserResponse[UserResponse["No"] = 0] = "No";
    UserResponse[UserResponse["Yes"] = 1] = "Yes";
})(UserResponse || (UserResponse = {}));
function respond(recipient, message) {
}
respond("John", UserResponse.Yes);
// classes
var User = /** @class */ (function () {
    function User(name, age) {
        this.name = name;
        this.age = age;
    }
    return User;
}());
var user = new User('ABC', 30);
//console.log(user.name, user.age) // age is not valid as it is outside class and has private modifier
console.log(user.name);
// above code can be written as below also
var User1 = /** @class */ (function () {
    function User1(name, age) {
        this.name = name;
        this.age = age;
        this.name = name; // also can remove this
        this.age = age; // also can remove this
    }
    return User1;
}());
var Admin = /** @class */ (function (_super) {
    __extends(Admin, _super);
    function Admin(name, age, permissions) {
        var _this = _super.call(this, name, age) || this;
        _this.permissions = permissions;
        return _this;
    }
    return Admin;
}(User1));
var PersonalDetail = /** @class */ (function () {
    function PersonalDetail(name) {
        this.name = name;
    }
    PersonalDetail.prototype.print = function () {
        console.log(this.name);
    };
    return PersonalDetail;
}());
//generic types
function logAndEcho(val) {
    console.log(val);
    return val;
}
//logAndEcho('Hi, there').split('') // will give error as string type not defined
logAndEcho('Hi, there').split('');
