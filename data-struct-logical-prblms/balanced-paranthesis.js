const openBrackets = ['(','{','[']
const closedBrackets = [')','}',']']
let expr = '[{}]'
let temp = []

for(i=0; i< expr.length; i++){
    if(openBrackets.includes(expr[i])){
        temp.push(expr[i])
    } else if(closedBrackets.includes(expr[i])){
        const openPair = closedBrackets.indexOf(expr[i])
        if(openBrackets[openPair] == temp[temp.length - 1]){
            temp.splice(-1, 1)
        } else {
            temp.push(expr[i])
        }
    }
}
if(temp.length == 0) {
    console.log('expression is balanced')
} else {
    console.log('expression is unbalanced')
}